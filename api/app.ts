import * as bodyParser from "body-parser";
import * as express from "express";
import * as child from "child_process";
import * as fs from "fs";
import * as rp from "request-promise";
import * as cheerio from "cheerio";
import * as uniqid from "uniqid";
import * as morgan from "morgan";

import configCors from "./config/cors";

const app = express();
configCors(app);

// this.app.use(morgan("combined", { stream: new MorganStreamer() }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/public', express.static('/usr/share/nginx/html/public/'));

app.use(morgan("combined"));
const redditVideoUrlRegex = new RegExp('^(https?:\\/\\/)v\\.redd\\.it\\/([\\w\\d]*)\\/HLSPlaylist\\.m3u8$');
const redditRegex = new RegExp('^(https?:\\/\\/)(www.)?(old.)?reddit.com\\/r\\/[a-zA-Z\\d\\/_]*$');
const twitterRegex = new RegExp('^https:\\/\\/twitter.com\\/.*\\/status\\/.*$')
app.post("/clip", (req: express.Request, res: express.Response) => {
  let website = "";
  if (req.body.uri.indexOf("reddit") > 0 && !redditRegex.test(req.body.uri) ) {
    res.statusCode = 400;
    res.send({status: "error", error: "Not a valid Reddit url"});
    return;
  } else if (req.body.uri.indexOf("twitter") > 0 && !twitterRegex.test(req.body.uri)) {
    res.statusCode = 400;
    res.send({status: "error", error: "Not a valid Twitter url"});
    return;
  } else if (req.body.uri.indexOf("twitter") > 0) {
    website = "twitter";
  } else if (req.body.uri.indexOf("reddit") > 0) {
    website = "reddit";
  } else {
    res.statusCode = 400;
    res.send({status: "error", error: "Provide a Twitter or Reddit url"});
    return;
  }

  const format = req.body.format == "mp4" ? "mp4" : "gif";

  const options = {
    uri: req.body.uri,
    transform: (body: any) => cheerio.load(body)
  }

  rp(options)
    .then(($: any) => {
      const id = uniqid();
      const mp4Path = `/usr/share/nginx/html/public/${id}.mp4`;
      const gifPath = `/usr/share/nginx/html/public/${id}.gif`;
      let uri = "";
      if (website === "reddit") {
        uri = GetRedditVideoUrl($, res);
      } else if (website === "twitter") {
        uri = GetTwitterUrl($, res);
      }
      if (!uri) {
        return;
      }
      child.exec(`youtube-dl '${uri}' ` +
        `-o ${mp4Path}`, (error: any, stdout: any, stderr: any) => {
          if (error) {
            console.error(error);
            if (res.headersSent) {
              fs.writeFile(`/tmp/clip/${id}.error`, error, console.error);
            } else {
              res.statusCode = 500;
              res.send({status: "error", error});
            }
            return;
          }
          if (format == "mp4") {
            return;
          }

          const command = `ffmpeg -y -i ${mp4Path} -vf fps=15,scale=480:-1:flags=lanczos,palettegen ${gifPath}.png && ffmpeg -i ${mp4Path} -i ${gifPath}.png -filter_complex "fps=15,scale=480:-1:flags=lanczos[x];[x][1:v]paletteuse" -f gif ${gifPath}.tmp`;
          console.log(command);
          child.exec(command, (err) => {
          // child.exec(`gifify ${mp4Path} -o ${gifPath}.tmp --resize 600:-1 --colors 128`, (err, stdout, stderr) => {
            if (err) {
              console.error(err);
              if (res.headersSent) {
                fs.writeFile(`/tmp/clip/${id}.error`, err, console.error);
              } else {
                res.statusCode = 500;
                res.send({status: "error", error});
              }
              return;
            }
            fs.rename(`${gifPath}.tmp`, gifPath, console.error);
          })
        });

        setTimeout(() => {
          res.statusCode = 202;
          res.send({id, status: "pending", format: format});
        }, 2000);
    })
    .catch((error) => {
      console.error(error);
      res.send({status: "error", error})
    });
});

function GetTwitterUrl($: any, res: express.Response) {
    var content = $("[property='og:image']").attr("content");
    var regex = new RegExp("^https://.*.twimg.com/tweet_video_thumb/(.*).jpg$")
    var match = content.match(regex);
    if (!match[1]) {
        res.statusCode = 400;
        res.send({status: "error", error: "Couldn't find a valid Twitter video"});
        return;
    } 
    return `https://video.twimg.com/tweet_video/${match[1]}.mp4`;
}

function GetRedditVideoUrl($: any, res: express.Response) {
    let uri = $("source").attr("src");
    if (!redditVideoUrlRegex.test(uri)) {
      const oldUri = $(".reddit-video-player-root").first().data("hls-url"); // todo: this doesn't work
      if (!redditVideoUrlRegex.test(oldUri)) {          
        res.statusCode = 400;
        res.send({status: "error", error: "Couldn't find a valid Reddit video"});
        return;
      }
      uri = oldUri;
    } 
    return uri;
}

app.get("/result/:id/:format?", (req, res) => {
  const id = req.params.id;
  const format = req.params.format == "mp4" ? "mp4" : "gif";
  const errorPath = `/tmp/clip/${id}.error`;
  const filePath = `/usr/share/nginx/html/public/${id}.${format}`;

  fs.exists(`/tmp/clip/${id}.error`, (exist) => {
    if (exist) {
      const error = fs.readFileSync(errorPath, "UTF-8");
      res.statusCode = 500;
      res.send({ status: "error", error });
    }
  });

  if (fs.existsSync(filePath)) {
      res.statusCode = 200;
      res.send({status: "done", filename: `/public/${id}.${format}`});
      return;
  } 

  if (fs.existsSync(`/usr/share/nginx/html/public/${id}.mp4`) && !res.headersSent) {
    res.statusCode = 202;
    res.send({id, status: "pending", format });
    return;
  }

  if (!res.headersSent) {
    res.statusCode = 202;
    res.send({status: "unknown", msg: "status unknown", id});
  }
})

app.listen(3000, () => console.log(`Example app listening on port 3000!`))