# vReddit sucks
I don't like vReddit. This makes vReddit videos into shareable gifs.

# Use
Drop a (new) Reddit video URL to the comments in de inputbox. 

For example: `https://reddit.com/r/aww/comments/90bu6w/heat_index_was_110_degrees_so_we_offered_him_a/`

# Run
## dev
```
cd api 
npm i
docker-compose build
docker-compose up
```
## prod
```
docker-compose build
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```